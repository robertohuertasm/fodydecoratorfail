﻿using Auth.Instrumentation.Interception;
using Autofac;

[module: LoggingInterceptor]

namespace Auth
{
    public class AuthModule : Module
    {
        [LoggingInterceptor]
        protected override void Load(ContainerBuilder builder)
        {
           
        }
    }
}
