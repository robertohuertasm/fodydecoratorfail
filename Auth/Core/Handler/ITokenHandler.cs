﻿using System.Threading.Tasks;
using Auth.Core.Contexts;

namespace Auth.Core.Handler
{
    public interface ITokenHandler
    {

        Task ProcessToken(TokenContext context);
    }
}
