﻿using System.Threading.Tasks;
using Auth.Core.Contexts;
using Auth.Instrumentation.Interception;

namespace Auth.Core.Handler
{
    public abstract class TokenHandler<TContext> : ITokenHandler where TContext : TokenContext
    {


        protected Task EmptyTask()
        {
            return Task.FromResult<object>(null);
        }

        [LoggingInterceptor]
        protected Task Unauthorized(TokenContext context)
        {
            context.Unauthorized();
            return EmptyTask();
        }

        [LoggingInterceptor]
        protected Task Authenticated()
        {
            return EmptyTask();
        }

        public Task ProcessToken(TokenContext context)
        {
            if (context is TContext)
            {
                return OnProcessToken(context as TContext);
            }

            return Task.FromResult<object>(null);
        }

        public abstract Task OnProcessToken(TContext context);
    }
}
