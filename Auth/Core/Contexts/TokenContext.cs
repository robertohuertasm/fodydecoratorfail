﻿using System.Collections.Generic;
using System.Security.Claims;
using Auth.Core.Enums;
using Auth.Store;

namespace Auth.Core.Contexts
{
    public abstract class TokenContext
    {
        protected const string AUTHENTICATION_TYPE = "JWT";

        public Audience Audience { get; private set; }
        public string User { get; protected set; }
        public AuthenticationType AuthenticationType { get; set; }
        public IDictionary<string, string> Headers = new Dictionary<string, string>();
        public ClaimsIdentity Identity { get; protected set; }
        public IDictionary<string, string> Properties { get; protected set; }
        public IDictionary<string, object> Payload { get; protected set; }
        public bool Failed { get; set; }
        public TokenContextError Error { get; set; }

        protected TokenContext(AuthenticationType authenticationType, Audience audience)
        {
            AuthenticationType = authenticationType;
            Audience = audience;
            Identity = new ClaimsIdentity(AUTHENTICATION_TYPE);
            Properties = new Dictionary<string, string>();
            Payload = new Dictionary<string, object>();
        }
    }
}
