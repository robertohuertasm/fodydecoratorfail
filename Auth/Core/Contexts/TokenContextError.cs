﻿namespace Auth.Core.Contexts
{
    public class TokenContextError
    {
        public int Code { get; private set; }
        public string Reason { get; private set; }

        public TokenContextError(int code, string reason)
        {
            Code = code;
            Reason = reason;
        }
    }
}
