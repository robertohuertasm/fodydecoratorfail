using Auth.Instrumentation.Interception;

namespace Auth.Core.Contexts
{
    public static class TokenContextExtensions
    {
        [LoggingInterceptor]
        public static void Unauthorized(this TokenContext context)
        {
            context.Failed = true;
            context.Error = new TokenContextError(401, "UNAUTHORIZED");
        }
    }
}