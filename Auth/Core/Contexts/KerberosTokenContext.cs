﻿using System.Security.Principal;
using Auth.Core.Enums;
using Auth.Store;

namespace Auth.Core.Contexts
{
    public class KerberosTokenContext : TokenContext
    {
        public IIdentity AdIdentity { get; private set; }

        public KerberosTokenContext(Audience audience, IIdentity identity): base(AuthenticationType.WindowsIntegrated, audience)
        {
            User = identity.Name;
            AdIdentity = identity;
        }
    }
}
