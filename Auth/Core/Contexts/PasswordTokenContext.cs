﻿using Auth.Core.Enums;
using Auth.Store;

namespace Auth.Core.Contexts
{
    public class PasswordTokenContext : TokenContext
    {
        public string Password { get; private set; }

        public PasswordTokenContext(Audience audience, string user, string password) : base(AuthenticationType.UserPassword, audience)
        {
            User = user;
            Password = password;
        }
    }
}
