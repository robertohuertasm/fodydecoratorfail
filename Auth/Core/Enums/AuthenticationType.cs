namespace Auth.Core.Enums
{
    public enum AuthenticationType
    {
        UserPassword,
        WindowsIntegrated
    }
}