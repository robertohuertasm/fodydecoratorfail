﻿namespace Auth.Core.Codes
{
    public struct PropertyKeys
    {
        public const string USER = "user";

        public const string AUDIENCE = "audience";
        public const string ISSUER = "issuer";
        public const string EXPIRES = "expired";
        public const string ISSUED = "issued";

        public const string ACCESS = "access";          // Acceso [access] (Cadena de caracteres de ‘0’ y ‘1’ formando la autorización del usuario a la aplicación)
        public const string SUBJECT = "sub";            // Nombre [subject]
        public const string DEPARTMENT = "dept";        // Departamento [department] (Código)
        public const string DISTRICT = "distr";         // Distrito [district]  (Código)
        public const string DIVISION = "div";           // División [division] (Código)
        public const string OPERATING_UNIT = "optun";   // Unidad operativa [operating unit] (Código)
        public const string CATEGORY = "cat";           // Categoría [category]
        public const string HOST_REGISTRY = "hreg";     // Matricula HOST [HOST registry] (Código)
        public const string DNI = "dni";                // DNI [dni]
        public const string EMAIL = "email";            // Email [email]
        public const string GROUPS = "groups";          // Grupos [groups] (Array)
    }
}