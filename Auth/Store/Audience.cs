﻿using System.Collections.Generic;
using Auth.Instrumentation.Interception;

namespace Auth.Store
{
    public class Audience
    {
        public string ClientId { get; set; }
        public long AccessTokenTTL { get; set; }
      
        public Dictionary<string, object> Bag { get; set; }

        [LoggingInterceptor]
        public string GetStringFromBag(string key)
        {
            if (Bag == null || !Bag.ContainsKey(key)) return null;
            return Bag[key].ToString();
        }
    }
}
