﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Auth.Instrumentation.Interception;
using Microsoft.Practices.EnterpriseLibrary.Logging;

[module: LoggingInterceptor]

namespace Auth.Instrumentation.Interception
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public class LoggingInterceptorAttribute : Attribute, IMethodDecorator
    {
        private MethodBase _method;
        private Dictionary<string, object> _params;
        protected Dictionary<string, object> Params
        {
            get { return _params ?? (_params = new Dictionary<string, object>()); }
        }
        
        public void Init(object instance, MethodBase method, object[] args)
        {
            _method = method;
           
            var parms = method.GetParameters();

            for (var i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                var par = parms[i];
                Params.Add(par.Name, arg);
            }
          
            Logger.Write($"Executing function <{method.Name}> at <{method.DeclaringType?.FullName}>", "Tracing", 1, 2027, TraceEventType.Information,"", Params);

        }

        public void OnEntry()
        {
            Logger.Write($"OnEntry function <{_method.Name}> at <{_method.DeclaringType?.FullName}>", "Tracing", 1, 2027, TraceEventType.Information, "", Params);
        }

        public void OnExit()
        {
            
        }

        public void OnException(Exception exception)
        {
            Params.Add("exception", exception);
            Logger.Write($"Error executing <{_method.Name}> at <{_method.DeclaringType?.FullName}>: {exception.Message}", "Tracing", 1,2027, TraceEventType.Error, "Exception", Params);
        }

        public void TaskContinuation(Task task)
        {
            Logger.Write($"TaskContinuation function <{_method.Name}> at <{_method.DeclaringType?.FullName}>", "Tracing", 1, 2027, TraceEventType.Information, "", Params);
        }
    }
}


