﻿using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Auth.Instrumentation.Interception
{
    public interface IMethodDecorator
    {
        void Init(object instance, MethodBase method, object[] args);

        void OnEntry();

        void OnExit();

        void OnException(Exception exception);

        void TaskContinuation(Task task);
    }
}
