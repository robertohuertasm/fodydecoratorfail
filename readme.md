## Purpose

This repo's purpose is to serve as help in order to solve this bug:
https://github.com/Fody/MethodDecorator/issues/8 from Fody's MethodDecorator project.

## How to proceed

Compile the project in release and try to decompile the Auth.TokenIssuer dll. There you will find a class named CommonTokenHandler.cs.

Open the code and you will see a couple of comments suggesting actions that will make the project compile flawlessly.