﻿namespace Auth.TokenIssuer
{
    public class TokenIssuerKeys
    {
        public const string USERNAME = "auth:Username";
        public const string AUDIENCE = "auth:Audience";
        public const string ISSUER = "auth:Issuer";
        public const string CORS_ORIGIN_HEADER_KEY = "Access-Control-Allow-Origin";
        public const string CORS_ORIGIN_HEADER_DEFAULT_VALUE = "*";
    }
}