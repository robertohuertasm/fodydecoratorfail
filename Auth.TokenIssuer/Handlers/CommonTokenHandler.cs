﻿using System;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;
using Auth.Core.Codes;
using Auth.Core.Contexts;
using Auth.Core.Handler;
using Auth.Instrumentation.Interception;

namespace Auth.TokenIssuer.Handlers
{
    
    public class CommonTokenHandler : TokenHandler<TokenContext>
    {
        [LoggingInterceptor]
        public override Task OnProcessToken(TokenContext context)
        {
            var identity = context.Identity;
            var properties = context.Properties;
            
            if (string.IsNullOrWhiteSpace(context.Audience.ClientId))
            {
                //IF YOU COMMENT THIS LINE IT WILL WORK
                return Unauthorized(context);
            }

            var issuer = ConfigurationManager.AppSettings.Get(TokenIssuerKeys.ISSUER);
            var accessTokenTTL = DateTimeOffset.UtcNow + TimeSpan.FromSeconds(context.Audience.AccessTokenTTL);

            //IF YOU COMMENT THIS LINE IT WILL WORK
            identity.AddClaim(new Claim(PropertyKeys.USER, context.User));

            properties.Add(PropertyKeys.AUDIENCE, context.Audience.ClientId);
            properties.Add(PropertyKeys.ISSUER, issuer);
            properties.Add(PropertyKeys.EXPIRES, accessTokenTTL.ToString());

            return Authenticated();
        }
    }
}