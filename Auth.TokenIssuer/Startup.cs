﻿using System.Web.Http;
using Auth.Instrumentation.Interception;
using Autofac;
using Owin;

[module: LoggingInterceptor]

namespace Auth.TokenIssuer
{
    public class Startup
    {
      

        public void Configuration(IAppBuilder app)
        {
           
       } 

        [LoggingInterceptor]
        public void ConfigureOAuth(IAppBuilder app)
        {
          
        }

        
        private void ConfigureContainer(IAppBuilder app, HttpConfiguration config)
        {
            
        }

        
        private void RegisterServices(ContainerBuilder builder)
        {
            
        }
    }
}